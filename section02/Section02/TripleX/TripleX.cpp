
#include "pch.h"
#include <iostream>
#include <ctime> // Preprocessor directives

void PrintIntroduction(int Difficulty)
{ /*
	std::cout << "|****| |****|" << std::endl;
	std::cout << "|****| |****|" << std::endl;
	std::cout << "*---|   |---*" << std::endl;
	std::cout << "**|       |**" << std::endl;
	std::cout << "***-------***" << std::endl;
	std::cout << "*************" << std::endl;
	std::cout << "*************" << std::endl;
	std::cout << "******A******" << std::endl;
	std::cout << "*****AAA*****" << std::endl;
	std::cout << "****AAAAA****" << std::endl;
	std::cout << "***AAAAAAA***" << std::endl;
	std::cout << "**AAAAAAAAA**" << std::endl;
	std::cout << "*AAAAAAAAAAA*" << std::endl;
	*/
	std::cout << "You are a secret agent breaking into a level " << Difficulty;
	std::cout << " secure server room ... Enter the correct code to continue ..." << std::endl; // Expression Statements
}


bool PlayGame(int Difficulty)
{
	PrintIntroduction(Difficulty);
	
	/*
	Declatation Statements, declare 3 number
	*/
	const int CodeA = rand() % Difficulty + Difficulty;
	const int CodeB = rand() % Difficulty + Difficulty;
	const int CodeC = rand() % Difficulty + Difficulty;

	const int CodeSum = CodeA + CodeB + CodeC;
	const int CodeProduct = CodeA * CodeB * CodeC;

	// Expression Statements
	std::cout << std::endl; std::cout << std::endl;
	std::cout << "There are 3 numbers in the code :" << std::endl;
	std::cout << "The code add-up to : " << CodeSum << std::endl;
	std::cout << "The code multiply to give : " << CodeProduct << std::endl;

	int GuessA, GuessB, GuessC;
	std::cin >> GuessA;
	std::cin >> GuessB;
	std::cin >> GuessC;

	const int GuessSum = GuessA + GuessB + GuessC;
	const int GuessProduct = GuessA * GuessB * GuessC;

	if (GuessSum == CodeSum && GuessProduct == CodeProduct) {
		std::cout << "*** Well done agent! You have extracted a file! Keep going! ***" << std::endl;
		return true;
	}
	else {
		std::cout << "*** You entered the wrong code! Careful agent! Try Again! ***" << std::endl;
		return false;
	}
}

int main() // main function
{
	srand(time(NULL)); // create new random sequence based on time of day

	int LevelDifficulty = 1;	
	int const MaxDifficulty = 5;

	while (LevelDifficulty <= MaxDifficulty) // Loop game until all levels complete
	{
		
		bool bLevelComplete = PlayGame(LevelDifficulty);
		std::cin.clear(); // Clears any errors
		std::cin.ignore(); // Discads the buffer

		if (bLevelComplete) 
		{
			// Increase the level difficulty
			++LevelDifficulty;
		}
		
	}
	
	std::cout << "\n*** Great work agent! You got all the files! Now Get out of there! ***\n";
	return 0;


}


 