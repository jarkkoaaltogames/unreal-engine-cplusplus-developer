// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SECTION02_Section02GameModeBase_generated_h
#error "Section02GameModeBase.generated.h already included, missing '#pragma once' in Section02GameModeBase.h"
#endif
#define SECTION02_Section02GameModeBase_generated_h

#define Section02_Source_Section02_Section02GameModeBase_h_15_RPC_WRAPPERS
#define Section02_Source_Section02_Section02GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Section02_Source_Section02_Section02GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASection02GameModeBase(); \
	friend struct Z_Construct_UClass_ASection02GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASection02GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Section02"), NO_API) \
	DECLARE_SERIALIZER(ASection02GameModeBase)


#define Section02_Source_Section02_Section02GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASection02GameModeBase(); \
	friend struct Z_Construct_UClass_ASection02GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASection02GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Section02"), NO_API) \
	DECLARE_SERIALIZER(ASection02GameModeBase)


#define Section02_Source_Section02_Section02GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASection02GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASection02GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASection02GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASection02GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASection02GameModeBase(ASection02GameModeBase&&); \
	NO_API ASection02GameModeBase(const ASection02GameModeBase&); \
public:


#define Section02_Source_Section02_Section02GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASection02GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASection02GameModeBase(ASection02GameModeBase&&); \
	NO_API ASection02GameModeBase(const ASection02GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASection02GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASection02GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASection02GameModeBase)


#define Section02_Source_Section02_Section02GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Section02_Source_Section02_Section02GameModeBase_h_12_PROLOG
#define Section02_Source_Section02_Section02GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Section02_Source_Section02_Section02GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Section02_Source_Section02_Section02GameModeBase_h_15_RPC_WRAPPERS \
	Section02_Source_Section02_Section02GameModeBase_h_15_INCLASS \
	Section02_Source_Section02_Section02GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Section02_Source_Section02_Section02GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Section02_Source_Section02_Section02GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Section02_Source_Section02_Section02GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Section02_Source_Section02_Section02GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Section02_Source_Section02_Section02GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SECTION02_API UClass* StaticClass<class ASection02GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Section02_Source_Section02_Section02GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
