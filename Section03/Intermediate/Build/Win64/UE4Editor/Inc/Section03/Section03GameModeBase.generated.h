// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SECTION03_Section03GameModeBase_generated_h
#error "Section03GameModeBase.generated.h already included, missing '#pragma once' in Section03GameModeBase.h"
#endif
#define SECTION03_Section03GameModeBase_generated_h

#define Section03_Source_Section03_Section03GameModeBase_h_15_RPC_WRAPPERS
#define Section03_Source_Section03_Section03GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Section03_Source_Section03_Section03GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASection03GameModeBase(); \
	friend struct Z_Construct_UClass_ASection03GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASection03GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Section03"), NO_API) \
	DECLARE_SERIALIZER(ASection03GameModeBase)


#define Section03_Source_Section03_Section03GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASection03GameModeBase(); \
	friend struct Z_Construct_UClass_ASection03GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASection03GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Section03"), NO_API) \
	DECLARE_SERIALIZER(ASection03GameModeBase)


#define Section03_Source_Section03_Section03GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASection03GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASection03GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASection03GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASection03GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASection03GameModeBase(ASection03GameModeBase&&); \
	NO_API ASection03GameModeBase(const ASection03GameModeBase&); \
public:


#define Section03_Source_Section03_Section03GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASection03GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASection03GameModeBase(ASection03GameModeBase&&); \
	NO_API ASection03GameModeBase(const ASection03GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASection03GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASection03GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASection03GameModeBase)


#define Section03_Source_Section03_Section03GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Section03_Source_Section03_Section03GameModeBase_h_12_PROLOG
#define Section03_Source_Section03_Section03GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Section03_Source_Section03_Section03GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Section03_Source_Section03_Section03GameModeBase_h_15_RPC_WRAPPERS \
	Section03_Source_Section03_Section03GameModeBase_h_15_INCLASS \
	Section03_Source_Section03_Section03GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Section03_Source_Section03_Section03GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Section03_Source_Section03_Section03GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Section03_Source_Section03_Section03GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Section03_Source_Section03_Section03GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Section03_Source_Section03_Section03GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SECTION03_API UClass* StaticClass<class ASection03GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Section03_Source_Section03_Section03GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
