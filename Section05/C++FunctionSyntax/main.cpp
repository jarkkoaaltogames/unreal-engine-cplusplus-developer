/*
  Write the minimal C++ Program
  - use given function DoubleMe
  - return type is int (short for integer)
  - Function name is main
  - takes no parameters
  - extra credit make it return 0
  - Test by running and see if the error goes away
*/

#include<iostream>

int DoubleMe(int number)
{
	return number * 2;
}

int main() {
	int ans;
	ans = DoubleMe(3);
	std::cout <<"Aswer is : "<< ans << std::endl;
	std::cin.get();
	return 0;
}