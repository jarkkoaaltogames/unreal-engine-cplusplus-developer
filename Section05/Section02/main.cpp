#include<iostream>
#include<string>

int main()
{

	// introduction the game 
	constexpr int WORD_LENGTH = 9;
	std::cout << "Welcome to Bulls and Cows a fun word game." << std::endl;
	std::cout << "Can you guess the " << WORD_LENGTH;
	std::cout << "\nLetter isogram I'm thinking of?\n" << std::endl;

	// get a guess from the player
	std::cout << "Enter your Guess: ";
	std::string Guess = "";
	std::cin >> Guess;

	// repeat the guess back to them
	std::cout << "You typed :" <<Guess << std::endl;

	std::cin.get();
	return 0;
}