#include<iostream>

int main()
{
	constexpr int WORD_LENGTH = 5;
	std::cout << "Welcome to Bulls and Cows a fun word game." << std::endl;
	std::cout << "Can you guess the " << WORD_LENGTH;
	std::cout << "Letter isogram I'm thinking of?\n" << std::endl;
	std::cin.get();
	return 0;
}